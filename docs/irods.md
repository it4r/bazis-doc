# Connect to VU Yoda with icommands

iRODS/Yoda is a middleware and webinterface to store enriched data.
On BAZIS the icommands are available to allow direct access.

To use the icommands a configuration file in your home folder is required.
https://yoda.vu.nl/site/getting-started/icommands.html#configuration

 
After configuration use ` iinit` to connect, use a  Data Access Password, similar to a  WebDAV connection.
 
Find an overview of the icommands here:
https://docs.irods.org/master/icommands/user/
Check the  `ipwd`, `ils`, `icd`, `iput`, `iget` en `irsync` commands for navigation and data transfer.

Your project folder is located at `/vu/home`

[icommands](https://docs.irods.org/master/icommands/user/)
[Yoda-VU](https://yoda.vu.nl)
