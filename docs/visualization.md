# Scientific Visualization
Researchers often spend large amount of time writing boilerplate code for simple tasks such as visualizing and analyzing large datasets for their research. Although there are a number of existing R or Python packages for specific purpose, it is often difficult for researchers with less programming experience to choose the right package, not to mention using the package.

## List of resources

* [A collection of Jupyter notebooks for geoscientists](https://github.com/pinshuai/jupyter-for-geoscience)

