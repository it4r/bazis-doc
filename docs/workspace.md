# Workspace

## scratch tmpdir

Each slurm job will have a fast scratch dir allocated on the nodes which is deleted after finishing the job. use the $TMPDIR virable to use this space for example to store intermediate results or work on many files. 

## scratch-shared (experimental)

Use the workspace tools on the beegfs parallel filesystem to create a project space.
The project space is a directory, with an associated expiration date, created on behalf of a user, to prevent disks from uncontrolled filling. 
The beegeefs parallel filesystem is faster than your usual NFS home space, but not backed up, so ideal for data which is easily recreated.

Your project space lives in the filesystem under: /scratch-shared/ws 

The project space is managed with the hpc-workspace tooling 
You can add them to your environment with: `module load hpc-workspace`

Example: setup a workspace "MyData" in a batchjob for 10 days.

```
SCR=$(ws_allocate MyData 10)
cd $SCR
```

Check your workspaces

```
$ ws_list 
id: MyData
     workspace directory  : /scratch-shared/ws/username-MyData
     remaining time       : 9 days 23 hours
     creation time        : Wed Mar 13 23:51:57 2013
     expiration date      : Sat Mar 23 23:51:57 2013
     available extensions : 15
```

Release the project space with

```
ws_release MyData
```

For user guide see https://github.com/holgerBerger/hpc-workspace/blob/master/user-guide.md
