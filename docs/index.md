# BAZIS HPC user documentation  

The BAZIS is a compute cluster for research at the Vrije Universiteit Amsterdam. It provides a service between the general facilties at the SURF HPC center and the desktop. It is a heterogenious system composed of clusters and servers from research departments and a general partition.

Here you will find information to get you started and best practices.
Clustercomputing can be very powerfull and usefull skill to add to your toolbox and get more science done.

Information and how to get an account can be found on [VU Service Portal](https://services.vu.nl/esc) under IT > Research > HPC Cluster Computing 

BAZIS is maintained by IT voor Onderzoek at the IT department

...

![VU BAZIS Cluster](img/HPCVUbazis.png)
