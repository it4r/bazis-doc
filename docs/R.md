# R environment

R has many powerfull scientific packages and a strong community. 
Installing and maintaining packages for R can be hard. On BAZIS the Bioconductor suite is installed and can be loaded with the appropiate module environment.

When first running R on a Cluster some changes in the workflow are required making the transition from working interactively from a terminal to scripts in batchmode.

# Errors
Pretty much all the time we get errors. Errors can be simple e.g.syntax error, R/python version error or more complex e.g. a problem in our data. In either case, please pay attention to what the error says carefully, because often the solution is in that message or at least it is the starting point of the solution while debugging your code. If it is an error you have not seen before, simply google it. Often you will find a solution in websites like stackoverflow.

## Tips and Caveats

# Producing PNG graphics and X11 related plotting errors
The png() default device used the X11 driver, which is not avaialble in batch mode or remote operation. Adding the type="cairo" option to your code solves this issue.

Example:

```
pdf(file = "testR.pdf", width = 4, height = 4)
plot(x = 1:10, y = 1:10)
abline(v = 0 )
text(x=0, y=1, labels = "random text")
dev.off()
```

```
png(file = "testR.png", type="cairo", width = 4, height = 4)
plot(x = 1:10, y = 1:10)
abline(v = 0 )
text(x=0, y=1, labels = "random text")
dev.off()
```

References
* [How do I produce PNG graphics in batch mode?](https://cran.r-project.org/doc/FAQ/R-FAQ.html#How-do-I-produce-PNG-graphics-in-batch-mode_003f)
