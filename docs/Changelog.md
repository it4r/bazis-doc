# Changelog

* 2023-07-19 Added Gromacs MD program 2023.1 GPU and CPU versions in the  2022 software stack.
* 2023-11-17 Added a new build of Bioconductor (version 3.16-foss-2022b-R-4.2.2) in the new 2023 stack.
