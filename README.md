# README #

This repository is published on https://bazis.readthedocs.io/en/latest/.

Current owner is Peter Vos. 
If anyone needs to take over the access:
1. sign up with your bitbucket account, this means you can login with your VU account.

2. Then import a "project" and choose this repo. 

3. Don't forget to change the slug to bazis.

4. Check https://docs.readthedocs.io/en/stable/guides/setup/git-repo-manual.html#manual-integration-setup on how to trigger an automatic rebuild on Readthedocs.